## Description
Information retrieval system 

## functional requirement
* Advance search
* Spell checker

## explanation && examples  
>index flow
![](docs/index-flow.png)

>search flow
![](docs/search-flow-diagram.png)

>search example
![](docs/search-example.png)

>spell check example
![](docs/spellchecker-example.png)

## Used tools and techniques
* Mysql(8.0)
* Java(1.8)
* spring-boot(2.2.4)
* [Boolean_model_of_information_retrieval](https://en.wikipedia.org/wiki/Boolean_model_of_information_retrieval)
* [Tf-idf](https://towardsdatascience.com/tf-idf-for-document-ranking-from-scratch-in-python-on-real-world-dataset-796d339a4089?gi=cfe4b518e01b#:~:text=TF%2DIDF%20stands%20for%20%E2%80%9CTerm,Information%20Retrieval%20and%20Text%20Mining.)
