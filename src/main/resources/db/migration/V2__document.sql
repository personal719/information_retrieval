create table document
(
  id int primary key auto_increment ,
  document_id varchar (100) not null unique ,
  boolean_vector LONGTEXT,
  term_vector LONGTEXT,
  tf_idf_vector LONGTEXT
);