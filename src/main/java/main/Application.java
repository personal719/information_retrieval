package main;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import main.search.SearchResult;
import main.search.TfIdfSearch;
import main.tokenizer.TokenizerPositionDecorator;
import main.tokenizer.TokenizerStemmingDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.apache.commons.lang3.StringUtils.join;


@SpringBootApplication
@RestController
@Slf4j
public class Application implements CommandLineRunner {

    @Autowired
    private AbstractIndexer indexer;
    @Autowired
    BooleanModelIndexer booleanModelIndexer;
    @Autowired
    TokenizerPositionDecorator decorator;

    @Autowired
    TokenizerStemmingDecorator stopWordsDecorator;
    @Autowired
    TokenCrud crud;
    @Autowired
    DocumentCrud documentCrud;
    @Autowired
    TokenizerStemmingDecorator tokenizerStemmingDecorator;
    @Autowired
    TfIdaModelVectorizedIndexer tfIdaModelVectorizedIndexer;
    @Autowired
    TfIdfSearch tfIdfSearch;
    ExecutorService pool = Executors.newFixedThreadPool(8);
    @Autowired
    private SpellChecker spellChecker;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

    }

    @GetMapping("/terms/info")
    public Object info() {
        Iterable<TermEntity> all = crud.findAll();
        return StreamSupport
                .stream(all.spliterator(), false)
                .map(f -> Container.getGson().fromJson(f.getBody(), Term.class));
    }

    @GetMapping("/spellchecker/spell")
    public Object spell(@RequestParam String q) throws JsonProcessingException {
        List<SpellResponse> collect = spellChecker.spell(q, crud.findAll()
                .stream().map(f -> f.getName())
                .collect(Collectors.toSet()))
                .stream()
                .map(f -> new SpellResponse(f.getKey(), f.getValue()))
                .collect(Collectors.toList());
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = mapper.schemaFor(SpellResponse.class).withHeader();
        return mapper
                .writerFor(List.class).with(schema)
                .writeValueAsString(collect);
    }

    @GetMapping(value = "/search", produces = "application/csv")
    public Object search(@RequestParam String q) throws JsonProcessingException {
        List<SearchResult> results = new TfIdfSearch(stopWordsDecorator, documentCrud, crud)
                .performSearch(q);
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = mapper.schemaFor(SearchResponse.class).withHeader();
        List<SearchResponse> collect = results.stream().map(f -> new SearchResponse().setId(f.getDocument()
                .getDocumentId())
                .setIndex(results.indexOf(f))
                .setScore(f.getScore())).collect(Collectors.toList());
        return mapper
                .writerFor(List.class).with(schema)
                .writeValueAsString(collect);
    }

    @Override
    public void run(String... args) throws Exception {
        int counter = 0;
        log.info("start indexing");
        List<Path> corpus = Files.walk(Paths.get("corpus"))
                .filter(Files::isRegularFile)
                .collect(Collectors.toList());
        for (Path path : corpus) {

            try {
                String source = join(new ArrayList<>(Files.readAllLines(path)), "");
                indexer.run(new Document()
                        .setDocumentId(path.toString())
                        .setSource(source));
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                System.out.println(path);
                System.exit(1000);
                e.printStackTrace();
            }
            log.info("finished from  path {} {}/{} count of vocabs {}", path, ++counter, corpus.size(), crud.count());
        }
        log.info("end indexing");
        long allDocuments = documentCrud.count();
        counter = 1;
        List<Future> futures = new ArrayList<>();
        for (Document document : documentCrud.findAll()) {
//
            Future<?> future = pool.submit(() -> {
                tfIdaModelVectorizedIndexer.run(document);
                log.info("Finished from  {}", document.getDocumentId());
            });

            futures.add(future);
        }
        for (Future future : futures) {
            future.get();
        }
        log.info("end tf-idf indexing");
    }


}

@AllArgsConstructor
@Data
class SpellResponse {
    private String word;
    private Integer dist;
}

@Data
@Accessors(chain = true)
class SearchResponse {
    private String id;
    private Double score;
    private Integer index;

}