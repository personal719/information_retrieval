package main;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import main.tokenizer.Tokenizer;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class TfIdfModelIndexer extends AbstractIndexer {
    @Setter
    private Tokenizer tokenizer;
    private final TokenCrud crud;
    private final DocumentCrud documentCrud;

    public TfIdfModelIndexer(Tokenizer tokenizer, TokenCrud crud, DocumentCrud documentCrud) {
        this.tokenizer = tokenizer;
        this.crud = crud;
        this.documentCrud = documentCrud;
    }

    @Override
    public void run(Document document) {
        getTfIDfIndexing(document);
        super.startNext(document);
    }

    @Transactional
    public void getTfIDfIndexing(Document document) {

        List<Term> terms = tokenizer.getTerms(document.getSource(), document.getDocumentId());
        List<Long> documentTermIds = new ArrayList<>();
        for (Term term : terms) {
            TermEntity termEntity = mergeOrSet(term);
            log.debug("[termEntity] {}", termEntity);
            crud.save(termEntity);
            documentTermIds.add(termEntity.getId());
        }

        document
                .setTermVector(documentTermIds.toString().replace(" ", ""));

        documentCrud.findByDocumentId(document.getDocumentId())
                .ifPresent((f) -> document.setId(f.getId()));
        documentCrud.save(document);
    }

    private TermEntity mergeOrSet(Term term) {
        try {
            Optional<TermEntity> termEntityInDb = crud.findByName(term.getTerm());
            if (termEntityInDb.isPresent()) {
                Term termInDb = Container.getGson().fromJson(termEntityInDb.get().getBody(), Term.class);
                filterPostingListById(termInDb, term.getPostingLists().get(0).getId());
                termInDb.setDocumentFreq(termInDb.getDocumentFreq() + 1);
                termInDb.getPostingLists().addAll(term.getPostingLists());
                termEntityInDb.get().setBody(termInDb.toJson());
                return termEntityInDb.get();
            } else {
                return new TermEntity()
                        .setName(term.getTerm())
                        .setBody(term.toJson());
            }
        } catch (Exception e) {
            System.out.println("Term failures is " + term.getTerm());
            e.printStackTrace();
        }
        return null;
    }

    private void filterPostingListById(Term termInDb, String id) {
        termInDb
                .setPostingLists(
                        termInDb.getPostingLists()
                                .stream()
                                .filter(f -> !f.getId().equals(id))
                                .collect(Collectors.toList()));
    }
}

