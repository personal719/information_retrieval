package main.tokenizer;

import main.Term;
import opennlp.tools.stemmer.PorterStemmer;

import java.util.List;

public class TokenizerStemmingDecorator extends TokenizerDecorator {
    public TokenizerStemmingDecorator(Tokenizer tokenizer) {
        super(tokenizer);
    }

    @Override
    public List<Term> getTerms(String text, String id) {
        List<Term> terms = super.getTerms(text, id);
        PorterStemmer stemmmer = new PorterStemmer();
        terms.forEach(s-> s.setTerm(stemmmer.stem(s.getTerm())));
        return terms;
    }
}
