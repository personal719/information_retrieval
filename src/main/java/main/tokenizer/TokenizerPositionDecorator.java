package main.tokenizer;

import main.Term;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TokenizerPositionDecorator extends TokenizerDecorator {

    public TokenizerPositionDecorator(Tokenizer tokenizer) {
        super(tokenizer);
    }

    @Override
    public List<Term> getTerms(String text, String id) {
        List<Term> terms = super.getTerms(text, id);
        return terms
                .stream()
                .map(f -> setPositions(terms, f))
                .collect(Collectors.toList());

    }

    private Term setPositions(List<Term> terms, Term term) {
        List<Integer> positions = IntStream
                .range(0, terms.size())
                .filter((i) -> terms.get(i).getTerm().equals(term.getTerm()))
                .boxed()
                .collect(Collectors.toList());
        term.getPostingLists().get(0)
                .setPositions(positions);
        return term;
    }
}

