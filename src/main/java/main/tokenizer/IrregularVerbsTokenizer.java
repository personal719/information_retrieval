package main.tokenizer;

import main.CustomFileReader;
import main.Term;

import java.util.List;
import java.util.Map;

public class IrregularVerbsTokenizer extends TokenizerDecorator {
    private Map<String, List<String>> verbsMap;

    public IrregularVerbsTokenizer(Tokenizer tokenizer) {
        super(tokenizer);
        verbsMap = CustomFileReader.getFileContent("irregularVerbs.txt");
    }

    @Override
    public List<Term> getTerms(String text, String id) {
        List<Term> terms = super.getTerms(text, id);
        terms.forEach(this::setRegularVerb);
        return terms;
    }

    private void setRegularVerb(Term term) {
        verbsMap.forEach((key, value) -> {
            if (value.indexOf(term.getTerm()) != -1)
                term.setTerm(key);
        });
    }
}
