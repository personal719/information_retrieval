package main.tokenizer;

import main.Term;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public abstract class Tokenizer {
    public abstract List<Term> getTerms(String text, String id);


    protected List<Term.PostingList> postingList(String term, String source, String id) {
        Term.PostingList postingList = new Term.PostingList();
        List<Term.PostingList> postingLists = new ArrayList<>();
        for (Integer index : findIndexes(term, source)) {
            int numFreq = StringUtils.countMatches(source, term);
            postingList
                    .setId(id)
                    .setNumFreq(numFreq);
            postingList
                    .addRange(new Term.PostingList
                            .Range()
                            .setStartIndex(index).setEndIndex(index + term.length()));

        }
        postingLists.add(postingList);
        return postingLists;
    }


    protected List<Integer> findIndexes(String term, String source) {
        int numIndex = 0;
        List<Integer> indexes = new ArrayList<>();
        int count = 0;
        while (source.indexOf(term, numIndex) != -1) {
            int e = source.indexOf(term, numIndex);
            indexes.add(e);
            numIndex += e + term.length();
        }
        return indexes;
    }

    protected Term getTerm(String term, String source, String id) {
        List<Term.PostingList> postingLists = postingList(term, source, id);
        return new Term()
                .setPostingLists(postingLists)
                .setDocumentFreq((long) postingLists.size())
                .setTerm(term);
    }
}
