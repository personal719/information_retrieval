package main.tokenizer;

import main.Term;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StopWordsDecorator extends TokenizerDecorator {
    public StopWordsDecorator(Tokenizer tokenizer) {
        super(tokenizer);
    }

    @Override
    public List<Term> getTerms(String text, String id) {
        try {
            List<String> stopWords = Files.readAllLines(Paths.get("StopWords"))
                    .stream()
                    .filter(f -> !f.isEmpty())
                    .map(String::trim)
                    .collect(Collectors.toList());
            List<String> punctuations = Stream.of(",", ".", "!", "?", "\"").collect(Collectors.toList());
            return super.getTerms(text, id)
                    .stream()
                    .filter(f -> !stopWords.contains(f.getTerm()))
                    .filter(f -> !punctuations.contains(f.getTerm()))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
