package main.tokenizer;

import main.Term;

import java.util.List;
import java.util.stream.Collectors;

public class TokenizerRidPunctuationDecorator extends TokenizerDecorator {
    public TokenizerRidPunctuationDecorator(Tokenizer tokenizer) {
        super(tokenizer);
    }

    @Override
    public List<Term> getTerms(String text, String id) {
        return super.getTerms(text, id)
                .stream()
                .map(t -> cleanTerm(t))
                .collect(Collectors.toList());
    }

    Term cleanTerm(Term t) {
        t.setTerm(t.getTerm()
                .replace(",", "")
                .replace("(", "")
                .replace(")", "")
                .replace("\"", ""));
        return t;
    }
}
