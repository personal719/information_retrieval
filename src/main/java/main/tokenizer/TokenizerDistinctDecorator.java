package main.tokenizer;

import main.Term;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TokenizerDistinctDecorator extends TokenizerDecorator {

    public TokenizerDistinctDecorator(Tokenizer tokenizer) {
        super(tokenizer);
    }

    @Override
    public List<Term> getTerms(String text, String id) {
        List<Term> terms = super.getTerms(text, id);
        return terms
                .stream()
                .filter(distinctByTerm(Term::getTerm))
                .distinct()
                .collect(Collectors.toList());
    }

    public <T> Predicate<T> distinctByTerm(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = new HashSet<>();
        return t -> seen.add(keyExtractor.apply(t));
    }
}
