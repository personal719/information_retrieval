package main.tokenizer;

import main.Term;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class NaiveTokenizer extends Tokenizer {

    public List<Term> getTerms(String source, String id) {
        List<String> terms = Arrays.stream(source
                .split(" +"))
                .filter(f -> !f.isEmpty()).collect(Collectors.toList());
        return terms
                .stream()
                .map(f -> getTerm(f, source, id))
                .collect(Collectors.toList());
    }
}

