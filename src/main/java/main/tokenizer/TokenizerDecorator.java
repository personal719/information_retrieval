package main.tokenizer;

import main.Term;

import java.util.List;

abstract class TokenizerDecorator extends Tokenizer {
    protected Tokenizer tokenizer;

    public TokenizerDecorator(Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }

    @Override
    public List<Term> getTerms(String text, String id) {
        return tokenizer.getTerms(text, id);
    }
}
