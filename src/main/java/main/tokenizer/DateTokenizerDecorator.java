package main.tokenizer;

import main.Term;
import main.date.DateFormatter;

import java.text.ParseException;
import java.util.List;

public class DateTokenizerDecorator extends TokenizerDecorator {
    public DateTokenizerDecorator(Tokenizer tokenizer) {
        super(tokenizer);
    }

    @Override
    public List<Term> getTerms(String text, String id) {
        List<Term> terms = super.getTerms(text, id);
        terms.forEach(term -> {
            try {
                String date = DateFormatter.convertToDefaultFormat(term.getTerm());
                term.setTerm(date);
            } catch (ParseException e) {
            }
        });
        return terms;
    }
}