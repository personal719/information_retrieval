package main.tokenizer;

import main.Term;
import main.CustomFileReader;

import java.util.List;
import java.util.Map;

public class SynonymsTokenizer extends TokenizerDecorator {
    private Map<String, List<String>> synonyms;

    public SynonymsTokenizer(Tokenizer tokenizer) {
        super(tokenizer);
        synonyms = CustomFileReader.getFileContent("synonyms.txt");
    }

    @Override
    public List<Term> getTerms(String text, String id) {
        List<Term> terms = super.getTerms(text, id);
        terms.forEach(this::setSynonym);
        return terms;
    }

    private void setSynonym(Term term) {
        synonyms.forEach((key, value) -> {
            if (value.indexOf(term.getTerm()) != -1)
                term.setTerm(key);
        });
    }
}