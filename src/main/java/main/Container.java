package main;

import com.google.gson.Gson;
import main.search.TfIdfSearch;
import main.tokenizer.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class Container {

    private final ApplicationContext context;

    public Container(ApplicationContext context) {
        this.context = context;
    }


    public static Gson getGson() {
        return new Gson();
    }

    @Bean
    NaiveTokenizer naiveTokenizer() {
        return new NaiveTokenizer();
    }

    @Bean
    TokenizerPositionDecorator getPositionTokenizer(NaiveTokenizer tokenizer) {
        return new TokenizerPositionDecorator(tokenizer);
    }

    @Bean
    TokenizerRidPunctuationDecorator getTokenizerRidPunctuationDecorator(NaiveTokenizer tokenizer) {
        return new TokenizerRidPunctuationDecorator(tokenizer);
    }

    @Bean
    TokenizerDistinctDecorator getDistinctTokenizer(TokenizerRidPunctuationDecorator tokenizer) {
        return new TokenizerDistinctDecorator(tokenizer);
    }


    @Bean
    StopWordsDecorator getStopWordTokenizer(TokenizerDistinctDecorator tokenizer) {
        return new StopWordsDecorator(tokenizer);
    }

    @Bean
    TokenizerStemmingDecorator getStemmingTokenizer(StopWordsDecorator tokenizer) {
        return new TokenizerStemmingDecorator(tokenizer);
    }

    @Bean
    IrregularVerbsTokenizer getIrregularVerbTokenizer(TokenizerStemmingDecorator tokenizer) {
        return new IrregularVerbsTokenizer(tokenizer);
    }

    @Bean
    DateTokenizerDecorator getDateTokenizer(IrregularVerbsTokenizer tokenizer) {
        return new DateTokenizerDecorator(tokenizer);
    }

    @Bean
    SynonymsTokenizer getSynonymsTokenizer(DateTokenizerDecorator tokenizer) {
        return new SynonymsTokenizer(tokenizer);
    }

    @Bean
    TfIdfSearch getTfIdfSearchModel(DocumentCrud crud, TokenCrud tokenCrud, TokenizerStemmingDecorator tokenizerStemmingDecorator) {
        return new TfIdfSearch(tokenizerStemmingDecorator, crud, tokenCrud);
    }


    @Bean("naiveTemplate")
    public TfIdfModelIndexer getNaiveIndexerTemplate(TokenCrud crud, NaiveTokenizer tokenizer, DocumentCrud documentCrud) {
        return new TfIdfModelIndexer(tokenizer, crud, documentCrud);
    }

    @Bean("tokenizerPositionDecorator")
    public TfIdfModelIndexer getTokenizerPositionDecorator(TokenCrud crud, TokenizerPositionDecorator tokenizer, DocumentCrud documentCrud) {
        return new TfIdfModelIndexer(tokenizer, crud, documentCrud);
    }

    @Bean("tokenizerDistinctDecorator")
    public TfIdfModelIndexer getTokenizerDistinctDecorator(TokenCrud crud, SynonymsTokenizer tokenizer, DocumentCrud documentCrud) {
        return new TfIdfModelIndexer(tokenizer, crud, documentCrud);
    }


    @Bean("booleanIndexing")
    public BooleanModelIndexer getBooleanIndexer(TokenCrud crud, DocumentCrud documentCrud) {
        return new BooleanModelIndexer(documentCrud, crud);
    }

    @Bean("tfIdaModelVectorizedIndexer")
    public TfIdaModelVectorizedIndexer getTfIdaModelVectorizedIndexer(TokenCrud crud, DocumentCrud documentCrud) {
        return new TfIdaModelVectorizedIndexer(documentCrud, crud);
    }

    @Bean
    @Primary
    public TfIdfModelIndexer getIndexer(@Value("${indexer.type}") String indexerQualifier, BooleanModelIndexer booleanModelIndexer) {
        TfIdfModelIndexer indexer = (TfIdfModelIndexer) context.getBean(indexerQualifier);
        return indexer;
    }

}
