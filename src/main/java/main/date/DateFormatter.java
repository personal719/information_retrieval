package main.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {
    private static final String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static String convertToDefaultFormat(String stringDate) throws ParseException {
        String format = DateUtil.determineDateFormat(stringDate);
        if (format == null)
            throw new ParseException("exception",1);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Date date = simpleDateFormat.parse(stringDate);
        simpleDateFormat.applyPattern(DEFAULT_FORMAT);
        return simpleDateFormat.format(date);
    }
}

