package main;

import lombok.Data;
import lombok.experimental.Accessors;
import main.Container;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class Term {
    private String term;
    private Long documentFreq;
    private List<PostingList> postingLists = new ArrayList<>();

    @Data
    @Accessors(chain = true)
    public static class PostingList {
        private Integer numFreq;
        private String id;
        private List<Integer> positions = new ArrayList<>();
        private List<Range> ranges = new ArrayList<>();

        @Data
        @Accessors(chain = true)
        public static class Range {
            Integer startIndex;
            Integer endIndex;
        }

        public PostingList addRange(Range range) {
            this.ranges.add(range);
            return this;
        }
    }

    public String toJson() {
        return Container.getGson()
                .toJson(this);
    }
}
