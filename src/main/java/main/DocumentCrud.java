package main;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DocumentCrud extends JpaRepository<Document, Long> {
    Optional<Document> findByDocumentId(String documentId);

}
