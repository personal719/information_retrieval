package main.search;

import main.Document;
import main.Term;
import main.tokenizer.Tokenizer;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Long.parseLong;

public abstract class AbstractSearch {
    protected Tokenizer tokenizer;

    public AbstractSearch(Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }

    public abstract List<SearchResult> performSearch(String query);

    protected Set<String> union(List<List<Term.PostingList>> p) {
        HashSet<String> setOfDocsIds = new HashSet<>();
        p.forEach(f -> f.forEach(k -> setOfDocsIds.add(k.getId())));
        return setOfDocsIds.stream()
                .filter(f -> anyDocumentMatch(p, f))
                .collect(Collectors.toSet());
    }

    protected boolean anyDocumentMatch(List<List<Term.PostingList>> p, String documentId) {
        return p.stream()
                .filter(f -> f.stream().anyMatch(e -> e.getId().equals(documentId)))
                .count() > 0;
    }

    private boolean allDocumentMatchId(List<List<Term.PostingList>> p, String documentId) {
        return p.stream()
                .filter(f -> f.stream().anyMatch(e -> e.getId().equals(documentId)))
                .count() == p.size();
    }

    protected double[] parseToArray(String v) {
        String[] split = v
                .replace("[", "")
                .replace("]", "")
                .split(",");
        double[] a = new double[split.length];
        for (int i = 0; i < split.length; i++) {
            a[i] = Double.parseDouble(split[i]);
        }
        return a;
    }

    protected Vector<Long> getDocumentsTermsIds(Document document) {
        String[] split = document.getTermVector()
                .replace("[", "")
                .replace("]", "")
                .split(",");
        Vector<Long> vector = new Vector<>();
        for (String s : split) vector.add(parseLong(s.trim()));
        return vector;
    }
}
