package main.search;

import lombok.extern.slf4j.Slf4j;
import main.*;
import main.tokenizer.Tokenizer;
import org.apache.commons.math3.util.MathArrays;
import org.apache.commons.math3.util.Precision;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class TfIdfSearch extends AbstractSearch {
    private final DocumentCrud documentCrud;
    private final TokenCrud tokenCrud;

    public TfIdfSearch(Tokenizer tokenizer, DocumentCrud documentCrud, TokenCrud tokenCrud) {
        super(tokenizer);
        this.documentCrud = documentCrud;
        this.tokenCrud = tokenCrud;
    }

    @Override
    public List<SearchResult> performSearch(String query) {
        List<Term> terms = tokenizer.getTerms(query, null);
        double[] vQuery = new double[(int) tokenCrud.count()];
        List<List<Term.PostingList>> postingLists = new ArrayList<>();
        for (Term term : terms) {
            tokenCrud.findByName(term.getTerm())
                    .ifPresent(f -> postingLists.add(Container.getGson()
                            .fromJson(f.getBody(), Term.class).getPostingLists()));
        }
//        List<Term.PostingList> intersect = intersect(postingLists);
        Set<String> intersect = union(postingLists);
        List<Document> documents = intersect
                .stream()
                .map(documentCrud::findByDocumentId)
                .collect(Collectors.toList())
                .stream()
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        List<SearchResult> results = new ArrayList<>();
        for (Document document : documents) {
            Arrays.fill(vQuery, 0, vQuery.length, 0);

            double[] vDoc = parseToArray(document.getTfIdfVector());
            for (Term term : terms)
                tokenCrud.findByName(term.getTerm())
                        .ifPresent(termEntity -> vQuery[termEntity.getId().intValue() - 1] = vDoc[termEntity.getId().intValue() - 1]);
            double v = Precision.round(MathArrays.cosAngle(vDoc, vQuery), 4);
//            log.info("[Query] {} result of vector {}", query, v);
            results.add(SearchResult.builder()
                    .document(document)
                    .score(v)
                    .build());
        }
        results.sort((o1, o2) -> o1.getScore() > o2.getScore() ? -1 : o1.getScore().equals(o2.getScore()) ? 0 : 1);
        return results
                .stream()
                .collect(Collectors.toList());
    }

}
