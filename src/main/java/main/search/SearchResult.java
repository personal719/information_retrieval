package main.search;

import lombok.Builder;
import lombok.Data;
import main.Document;

@Data
@Builder
public class SearchResult {
    private Document document;
    private Double score;
}
