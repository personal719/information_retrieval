package main.search;

import lombok.extern.slf4j.Slf4j;
import main.*;
import main.tokenizer.Tokenizer;
import org.apache.commons.math3.util.MathArrays;
import org.apache.commons.math3.util.Precision;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class BooleanSearch extends AbstractSearch {
    private final DocumentCrud documentCrud;
    private final TokenCrud tokenCrud;

    public BooleanSearch(Tokenizer tokenizer, DocumentCrud documentCrud, TokenCrud tokenCrud) {
        super(tokenizer);
        this.documentCrud = documentCrud;
        this.tokenCrud = tokenCrud;
    }

    @Override
    public List<SearchResult> performSearch(String query) {
//        List<Term> terms = tokenizer.getTerms(query, null);
//        double[] vQuery = new double[(int) tokenCrud.count()];
//        Arrays.fill(vQuery, 0, vQuery.length, 0);
//        terms.forEach(f -> log.info("Term {}", f.getTerm()));
//        for (Term term : terms)
//            tokenCrud.findByName(term.getTerm())
//                    .ifPresent(termEntity -> vQuery[termEntity.getId().intValue() - 1] = 1L);
//        log.info("[Query] {} result of vector {}", query, vQuery);
//        List<List<Term.PostingList>> postingLists = new ArrayList<>();
//        for (Term term : terms) {
//            tokenCrud.findByName(term.getTerm())
//                    .ifPresent(f -> postingLists.add(Container.getGson()
//                            .fromJson(f.getBody(), Term.class).getPostingLists()));
//        }
////        List<Term.PostingList> intersect = intersect(postingLists);
//        List<Term.PostingList> intersect = union(postingLists);
//        log.info("intersect docs {}", intersect);
//        List<Document> documents = intersect
//                .stream()
//                .map(f -> documentCrud.findByDocumentId(f.getId()))
//                .collect(Collectors.toList())
//                .stream()
//                .filter(Optional::isPresent)
//                .map(Optional::get)
//                .collect(Collectors.toList());
//        List<SearchResult> results = new ArrayList<>();
//        for (Document document : documents) {
//            double[] vDoc = parseToArray(document.getBooleanVector());
//            double v = Precision.round(MathArrays.cosAngle(vDoc, vQuery), 2);
//            results.add(SearchResult.builder()
//                    .document(document)
//                    .score(v)
//                    .build());
//        }
//        return results;
        return null;
    }

}

