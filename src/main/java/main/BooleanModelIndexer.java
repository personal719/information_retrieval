package main;

import lombok.AllArgsConstructor;

import java.util.Vector;

import static java.lang.Long.parseLong;

@AllArgsConstructor
public class BooleanModelIndexer extends AbstractIndexer {
    private final DocumentCrud crud;
    private final TokenCrud tokenCrud;


    @Override
    public void run(Document document) {
        long vectorLength = tokenCrud.count();
        Vector<Long> zeroVector = initZeroVector(vectorLength);
        vectorize(document, zeroVector);
        super.startNext(document);

    }

    private void vectorize(Document document, Vector<Long> initVector) {
        Vector<Long> documentTermsIds = getDocumentsTermsIds(document);
        for (Long termId : documentTermsIds) initVector.set(termId.intValue() - 1, 1L);
        document.setBooleanVector(initVector.toString());
        crud.save(document);
        for (Long termId : documentTermsIds) initVector.set(termId.intValue() - 1, 0L);
    }


}
