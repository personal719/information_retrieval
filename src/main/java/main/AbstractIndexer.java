package main;

import java.util.Vector;

import static java.lang.Long.parseLong;

public abstract class AbstractIndexer {
    private AbstractIndexer next;

    public AbstractIndexer linkWith(AbstractIndexer next) {
        this.next = next;
        return next;
    }

    public abstract void run(Document document);

    protected void startNext(Document document) {
        if (this.next != null)
            this.next.run(document);
    }

    protected Vector<Long> getDocumentsTermsIds(Document document) {
        String[] split = document.getTermVector()
                .replace("[", "")
                .replace("]", "")
                .split(",");
        Vector<Long> vector = new Vector<>();
        for (String s : split) vector.add(parseLong(s.trim()));
        return vector;
    }

    protected Vector initZeroVector(long vectorLength) {
        Vector vector = new Vector<>();
        for (long i = 0; i < vectorLength; i++) vector.add(0L);
        return vector;
    }


}
