package main;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import main.search.AbstractSearch;
import main.search.SearchResult;
import main.search.TfIdfSearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static java.lang.Integer.parseInt;
import static java.util.Arrays.asList;

@Component
@Slf4j
public class EvaluateRunner implements CommandLineRunner {
    @Autowired
    TfIdfSearch search;
    private final static Evaluate evaluate = new Evaluate();

    @Override
    public void run(String... args) throws Exception {
        Map<String, List<Integer>> dataSet = initDataToEvaluate();
        List<Evaluate.EvaluateRes> evaluateResults = new ArrayList<>();
        dataSet.entrySet()
                .forEach(e -> evaluateResults.add(evaluate.search(this.search, e.getKey(), e.getValue())));
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = mapper.schemaFor(Evaluate.EvaluateRes.class).withHeader();
        String value = mapper.writerFor(List.class)
                .with(schema)
                .writeValueAsString(evaluateResults);
        toFile(value, "evalute.csv");

    }

    private void toFile(String value, String path) throws IOException {
        Files.write(Paths.get(path), value.getBytes());
        log.info("Values stored in {}", new File(path).getAbsoluteFile());
    }

    private Map<String, List<Integer>> initDataToEvaluate() {
        HashMap<String, List<Integer>> map = new HashMap<>();
        map.put("KENNEDY ADMINISTRATION PRESSURE ON NGO DINH DIEM TO STOP SUPPRESSING THE BUDDHISTS ."
                , asList(268, 288, 304, 308, 323, 326, 334));//query #1
        map.put("  ARRANGEMENTS FOR INDONESIA TO TAKE OVER THE ADMINISTRATION " +
                "OF WEST IRIAN, WHICH HAS BEEN UNDER UNITED NATIONS ADMINISTRATION .", asList(195, 198));//query #11

        map.put(" CONTROVERSY BETWEEN INDONESIA AND MALAYA ON THE PROPOSED " +
                        "FEDERATION OF MALAYSIA, WHICH WOULD UNITE FIVE TERRITORIES .",
                asList(61, 155, 156, 242, 269, 339, 358));//query #10

        map.put(" EFFORTS OF THE THREE-NATION INTERNATIONAL CONTROL COMMISSION " +
                "FOR INDO-CHINA TO TRY TO STOP THE FIGHTING THAT HAS FLARED " +
                "IN LAOS .", asList(169, 170, 239)); //query #16
        map.put(" THE UNITED STATES HAS WARNED IT WOULD LIMIT ITS UNITED NATIONS " +
                " " +
                "PAYMENTS TO THE LEVEL OF ITS REGULAR ASSESSMENT IF NATIONS NOW IN " +
                " " +
                "ARREARS FAIL TO PAY UP . WHAT ISSUES ARE INVOLVED IN THESE " +
                " " +
                "NATIONS' BEING IN ARREARS .", asList(356));//query #20
        return map;
    }

    private static class Evaluate {
        public EvaluateRes search(AbstractSearch search, String q, List<Integer> allRelevant) {
            List<SearchResult> results = search.performSearch(q);
            return EvaluateRes.builder()
                    .precision(getPrecision(results, allRelevant))
                    .recall(getRecall(results, allRelevant))
                    .build();
        }

        double getPrecision(List<SearchResult> results, List<Integer> allRelevant) {
            return (allRelevant.size() * 1.0) / results.size();
        }

        double getRecall(List<SearchResult> results, List<Integer> allRelevant) {
            long relevant = results
                    .stream()
                    .filter(f -> allRelevant.contains(parseInt(f.getDocument()
                            .getDocumentId()
                            .replaceAll("[^0-9]", ""))))
                    .count();
            return (relevant * 1.0) / allRelevant.size();
        }

        @Data
        @Builder
        public static class EvaluateRes {
            double precision;
            double recall;
        }
    }
}

