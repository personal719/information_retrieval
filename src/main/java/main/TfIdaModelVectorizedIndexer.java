package main;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.apache.commons.math3.util.Precision;

import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import static java.lang.Math.log;

public class TfIdaModelVectorizedIndexer extends AbstractIndexer {

    private DocumentCrud documentCrud;
    private TokenCrud crud;
    private final long numOfDoc;

    public TfIdaModelVectorizedIndexer(DocumentCrud documentCrud, TokenCrud crud) {
        this.documentCrud = documentCrud;
        this.crud = crud;
        numOfDoc = documentCrud.count();
    }

    @Override
    public void run(Document document) {
        Vector initVector = initZeroVector(crud.count());
        vectoring(document, initVector);
    }


    private void vectoring(Document document, Vector<Double> initVector) {
        Vector<Long> documentTermsIds = getDocumentsTermsIds(document);
        for (Long termId : documentTermsIds)
            initVector.set(termId.intValue() - 1, calcTfIdf(document, crud.findById(termId).get(), numOfDoc));
        document.setTfIdfVector(initVector.toString());
        documentCrud.save(document);
        for (Long termId : documentTermsIds) initVector.set(termId.intValue() - 1, 0d);
    }

    private Double calcTfIdf(Document document, TermEntity termEntity, long numOfDoc) {
        Term term = Container.getGson().fromJson(termEntity.getBody(), Term.class);
        Term.PostingList post = term.getPostingLists()
                .stream()
                .filter(f -> f.getId().equals(document.getDocumentId()))
                .findFirst()
                .get();
        double tf = post.getNumFreq();
        double df = term.getPostingLists().size();
        tf = normalizeTf(tf, getDocumentsTermsIds(document).size(), 0.5);
        double idf = normalizeIDf(df, numOfDoc);
        double v = tf * idf;
        return Precision.round(v, 4);
    }

    private double normalizeTf(double tf, long numOfTerms, double k) {
        return 1 + log(tf);
    }

    private double normalizeIDf(double df, long numOfDoc) {
        return log(1 + (numOfDoc * 1.0) / (df));
    }
}
