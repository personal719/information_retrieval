package main;

import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SpellChecker {
    private final static int maxDistance = 2;

    private int dist(String string1, String string2) {
        char[] s1 = string1.toCharArray();
        char[] s2 = string2.toCharArray();
        int[][] d = new int[s1.length + 1][s2.length + 1];

        for (int i = 0; i < s1.length + 1; i++) {
            d[i][0] = i;
        }

        for (int j = 0; j < s2.length + 1; j++) {
            d[0][j] = j;
        }

        for (int i = 1; i < s1.length + 1; i++) {
            for (int j = 1; j < s2.length + 1; j++) {
                int d1 = d[i - 1][j] + 1;
                int d2 = d[i][j - 1] + 1;
                int d3 = d[i - 1][j - 1];
                if (s1[i - 1] != s2[j - 1]) {
                    d3 += 1;
                }
                d[i][j] = Math.min(Math.min(d1, d2), d3);
            }
        }
        return d[s1.length][s2.length];
    }

    public List<Map.Entry<String, Integer>> spell(String text, Set<String> hashSet) {
        HashMap<String, Integer> suggestions = new HashMap<>();
        hashSet.forEach(word -> {
            int dist = dist(text, word);
            if (dist <= maxDistance)
                suggestions.put(word, dist);
        });
        return sort(suggestions);
    }

    public List<Map.Entry<String, Integer>> sort(Map<String, Integer> suggestion) {
        return suggestion.entrySet().stream()
                .filter(f -> f.getValue() > 0)
                .sorted(Comparator.comparingInt(Map.Entry::getValue))
                .collect(Collectors.toList());
    }
}