package main;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomFileReader {
    private static BufferedReader reader;

    private static void readFile(String filePath) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(
                filePath));
    }

    public static Map<String, List<String>> getFileContent(String filePath)  {
        Map<String, List<String>> verbsMap = new HashMap<>();
        try {
            readFile(filePath);
            String line = reader.readLine();
            while (line != null) {
                String[] verbs = line.split(",");
                String[] strings = Arrays.copyOfRange(verbs, 1, verbs.length);
                verbsMap.put(verbs[0], Arrays.asList(Arrays.copyOfRange(verbs, 1, verbs.length)));
                line = reader.readLine();
            }
            reader.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return verbsMap;
    }
}
