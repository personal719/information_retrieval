package main;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TokenCrud extends CrudRepository<TermEntity, Long> {
    Optional<TermEntity> findByName(String name);

    List<TermEntity> findAll();
}
